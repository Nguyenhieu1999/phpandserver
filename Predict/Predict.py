import sys
import os
from joblib import dump, load
import constant
import pandas as pd

sys.path.append(os.path.abspath(constant.toSubLibpath()))

import pickle
import pandas as pd

import DataPreProcess
from StringProcess import StringProcess

import numpy as np

class Predict:
    def __init__(self, model_path):
        '''Example: MLPClassifier_model.joblib for model_path'''
        self.model_path = model_path
    
    def load_model(self):
        if os.name == 'nt':
            try:
                self.model = load(constant.toModelpath() + str(self.model_path))
            except FileNotFoundError:
                self.model = load(self.model_path)
        else:
            try:
                self.model = load(constant.toModelpath() + str(self.model_path))
            except FileNotFoundError:
                self.model = load(self.model_path)
    
    def load_input_from_array(self, data):
        first_products = data[:, 0]
        second_products = data[:, 1]

        self.processed_data = []
        stringProcess_obj = StringProcess()

        for s1, s2 in zip(first_products, second_products):
            self.processed_data.append(stringProcess_obj.string_vectorizer(s1, s2))
    
    def load_input_from_csv(self, csv_path):
        if os.name == 'nt':
            # data = np.array(pd.read_csv('D:/AE_Final/Train/' + csv_path, sep=",", header=None))
            pass
        else:
            try: 
                data = np.array(pd.read_csv(constant.toTrainpath() + csv_path, sep=","))
            except FileNotFoundError:
                data = np.array(pd.read_csv(csv_path, sep=","))
        first_products = data[:, 0]
        second_products = data[:, 1]

        self.processed_data = []
        stringProcess_obj = StringProcess()

        for s1, s2 in zip(first_products, second_products):
            self.processed_data.append(stringProcess_obj.string_vectorizer(s1, s2))
    
    def load_input_from_json(self, list_json):
        main_title = list_json[0]
        rest_titles = list_json[1]
        main_titles = [main_title] * len(rest_titles)
        self.json = list(zip(main_titles, rest_titles))
        self.processed_data = []
        stringProcess_obj = StringProcess()

        for s1, s2 in zip(main_titles, rest_titles):
            self.processed_data.append(stringProcess_obj.string_vectorizer(s1, s2))

    def predict(self):
        return self.model.predict(self.processed_data)
    
    def predict_proba(self):
        return self.model.predict_proba(self.processed_data) * 100

    def output_json(self):
        predictions = np.array(self.predict())
        predict_probabilities = np.max(np.array(self.predict_proba()), axis=1)
        
        result = []
        for pos in range(predictions.shape[0]):
            (s1, s2) = self.json[pos]
            result.append({'product_1' : s1, 'product_2' : s2, 'match' : predictions[pos], 'percent' : predict_probabilities[pos]})
        
        df = pd.DataFrame(result)

        print(df)

        return df



