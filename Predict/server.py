from http.server import BaseHTTPRequestHandler, HTTPServer
import logging
from Predict import Predict
import json 
class S(BaseHTTPRequestHandler):
    model = Predict('MLPClassifier_Model_13.joblib')
    received_data = str(input('Enter product:'))
    data = received_data.split(',')
    model.load_model()
    model.load_input_from_json(data)
    model.output_json()
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        logging.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.model), str(self.received_data))
        self._set_response()
        self.wfile.write("GET request for {}".format(self.model).encode('utf-8'))

    def do_POST(self):
        content_length = int(self.received_data['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        logging.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
                str(self.model), str(self.received_data), post_data.decode('utf-8'))

        self._set_response()
        self.wfile.write("POST request for {}".format(self.model).encode('utf-8'))

def run(server_class=HTTPServer, handler_class=S, port=8080):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')

if __name__ == '__main__':
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()