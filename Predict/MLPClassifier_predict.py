from Predict import Predict
import numpy as np
from sklearn.metrics import accuracy_score
import json


model = Predict('MLPClassifier_Model_13.joblib')
received_data = str(input('Enter product:'))
data = received_data.split(',')
 
model.load_model()

model.load_input_from_json(data)

model.output_json()
