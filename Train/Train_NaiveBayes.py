import sys
import os
import pandas as pd
import constant

sys.path.append(os.path.abspath(constant.toSubLibpath()))

from Train import TrainBase
from DataPreProcess import train_test_split

from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import MultinomialNB

class Training_MNB(TrainBase):
    # OVERRIDE
    def train_to_model(self, ratio=0.8, alpha=1.0, fit_prior=False, class_prior=None):
        
        self.ratio = ratio
        
        train_data, test_data, train_labels, test_labels = train_test_split(self.vectors, self.labels, int(self.size_of_data * ratio))
            
        model = MultinomialNB(alpha=alpha, fit_prior=fit_prior, class_prior=class_prior)
        
        model.fit(train_data, train_labels)

        self.accuracy = accuracy_score(model.predict(test_data), test_labels)
        self.model = model