import sys
import os
import constant

sys.path.append(os.path.abspath(constant.toTrainpath()))

from Train_SVC import Training_SVC

data_name = str(input("Enter data file name: "))

training = Training_SVC(data_name)
training.process_data()
training.train_to_model(kernel='rbf', C=100, gamma=1, class_weight='balanced')

print(training.print_acc())

save = input('Save model? Y/N ')
if save == 'Y':
    model = input('Enter where to save: ')
    print(training.print_and_save(model))